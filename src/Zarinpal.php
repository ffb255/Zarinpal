<?php
namespace ffb128\Zarinpal;

use ffb128\Zarinpal\Constants\WebServiceConstants;
use ffb128\Zarinpal\Interfaces\HttpInterface;
use ffb128\Zarinpal\Http\Response;

class Zarinpal
{
    private $merchantID;

    protected $amount = null;
    protected $description = null;
    protected $email = null;
    protected $mobile = null;
    protected $callback = null;
    protected $additionalData = [];

    private $requestResult = null;
    private $isZaringate = false;
    private $isSandbox = false;
    private $httpClient = null;

    public function __construct($merchantID) {
        $this->merchantID = $merchantID;
    }

    /**
     * Set Amount for payment
     *
     * @param  intiger $amount in IR Toman
     * @return void
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Set Description for payment
     *
     * @param  string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Set User Email for payment
     *
     * @param  string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Set User Mobile for payment
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Set Payment Callback URL (for redirection after payment)
     *
     * @param string $url
     * @return void
     */
    public function setCallback($url)
    {
        $this->callback = $url;
    }
    
    /**
     * Enable Zaringate Service
     * need to activate some necessary services from Zarinpal
     *
     * @return void
     */
    public function enableZaringate()
    {
        $this->isZaringate = true;
    }

    /**
     * Enable Sandbox (Test Mode)
     *
     * @return void
     */
    public function enableSandbox()
    {
        $this->isSandbox = true;
    }

    /**
     * Add a shared pay item (Shared pay off)
     * need to activate some necessary services from Zarinpal
     *
     * @param [type] $account
     * @param [type] $amount
     * @param [type] $description
     * @return void
     */
    public function addSharedPay($account, $amount, $description)
    {
        $this->additionalData['Wages'][$account] = [
            'Amount'=> $amount,
            'Description'=> $description,
        ];
    }

    /**
     * Set Authority expire time [1800,3888000]
     *
     * @param int $seconds
     * @return void
     */
    public function expireIn($seconds)
    {
        $this->additionalData['expireIn'] = $seconds;
    }

    /**
     * Generate Parameters Array
     *
     * @param array $parameters Custom Parameters
     * @return array
     */
    public function generateParameterArray($parameters = [])
    {
        
        $Amount = $this->amount;
        $Description = $this->description;
        $Email = $this->email;
        $Mobile = $this->mobile;
        $CallbackURL = $this->callback;
        $MerchantID = $this->merchantID;
        $AdditionalData = $this->additionalData;

        // Custom Output
        if(!empty($parameters)){
            $outputArray = [];
            foreach($parameters as $key=>$value){
                 if(!is_numeric($key)){
                    $outputArray = array_merge($outputArray, [
                        $key=>$value,
                    ]);
                 } else{
                    $outputArray = array_merge($outputArray, compact($value));
                 }
            }
            return $outputArray;
        }

        return compact("MerchantID", "Amount", "Description", "Email", "Mobile", "CallbackURL", "AdditionalData");
    }

    /**
     * Get Redirect URL (Payment gateway URL)
     *
     * @return string
     */
    public function getRedirectURL()
    {
        $zarinGate = ($this->isZaringate && !$this->isSandbox) ? "/zaringate" : "/";
        if($this->requestResult->ok){
            // Return Sandbox Redirect URL (if enabled)
            if($this->isSandbox){
                return WebServiceConstants::SANDBOX_BASE_REDIRECT_URL . $this->getAuthority() . $zarinGate;
            }
            // Return Production Redirect URL
            return WebServiceConstants::BASE_REDIRECT_URL . $this->getAuthority() . $zarinGate;
        } else{
            return false;
        }
    }

    /**
     * Get Authority
     *
     * @return string|int
     */
    public function getAuthority()
    {
        if(!$this->requestResult->ok){
            return false;
        }
        return $this->requestResult->body->Authority;
    }

    /**
     * Redirect user to payment URL
     *
     * @return void
     */
    public function redirect()
    {
        header('Location: '.$this->getRedirectURL());
    }

    /**
     * Set Http Client
     *
     * @param \ffb128\Zarinpal\Interfaces\HttpInterface $client
     * @return void
     */
    public function setHttpDriver(HttpInterface $client){
        $this->httpClient = $client;
    }

    /**
     * Get Http Client
     * by default Curl Driver will be used
     *
     * @return HttpInterface
     */
    private function getHttpDriver(){
        if($this->httpClient == null){
            $this->httpClient = new \ffb128\Zarinpal\Http\Drivers\Curl; 
        }
        return $this->httpClient;
    }

    /**
     * Send Request to get Authurity
     *
     * @return Response
     */
    public function request()
    {
        $parameters = $this->generateParameterArray();

        $method = empty($this->additionalData) ? WebServiceConstants::METHOD_REQUEST : WebServiceConstants::METHOD_REQUEST_WITH_EXTRA;
        $this->requestResult = $this->getHttpDriver()->post(
            $method,
            $parameters,
            $this->isSandbox
        );
        return $this->requestResult;
    }

    /**
     * Verify a Transaction
     *
     * @return Response
     */
    public function verify()
    {
        $authority = (isset($_GET['Authority']) ? $_GET['Authority'] : 'xxx');
        $parameters = $this->generateParameterArray([
            "MerchantID",
            "Amount",
            "Authority"=> $authority,
        ]);
        $this->requestResult = $this->getHttpDriver()->post(
            WebServiceConstants::METHOD_VERIFICATION,
            $parameters,
            $this->isSandbox
        );
        return $this->requestResult;
    }

    /**
     * Refresh an authority for custom seconds
     *
     * @param string $authority
     * @param int $expireIn In seconds
     * @return Response
     */
    public function refreshAuthority($authority, $expireIn){
        $parameters = $this->generateParameterArray([
            "MerchantID",
            "Authority"=> $authority,
            "ExpireIn"=> $expireIn,
        ]);
        $this->requestResult = $this->getHttpDriver()->post(
            WebServiceConstants::METHOD_REFRESH_AUTHORITY,
            $parameters,
            $this->isSandbox
        );
        return $this->requestResult;
    }

    /**
     * Get a list of unverified transactions (faild transaction list)
     *
     * @return Response
     */
    public function getUnverified(){
        $parameters = $this->generateParameterArray([
            "MerchantID",
        ]);
        $this->requestResult = $this->getHttpDriver()->post(
            WebServiceConstants::METHOD_UNVERIFIED_TRANSACTION,
            $parameters,
            false
        );
        return $this->requestResult;
    }

}
