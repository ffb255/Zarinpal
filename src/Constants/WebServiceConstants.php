<?php
namespace ffb128\Zarinpal\Constants;

class WebServiceConstants
{
    const REST_API_URL = 'https://www.zarinpal.com/pg/rest/WebGate/%s.json';
    const SOAP_API_URL = 'https://www.zarinpal.com/pg/services/WebGate/wsdl';
    const BASE_REDIRECT_URL = "https://www.zarinpal.com/pg/StartPay/";

    const SANDBOX_REST_API_URL = 'https://sandbox.zarinpal.com/pg/rest/WebGate/%s.json';
    const SANDBOX_SOAP_API_URL = 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl';
    const SANDBOX_BASE_REDIRECT_URL = "https://sandbox.zarinpal.com/pg/StartPay/";

    const METHOD_REQUEST = "PaymentRequest";
    const METHOD_REQUEST_WITH_EXTRA = "PaymentRequestWithExtra";
    const METHOD_VERIFICATION = "PaymentVerification";
    const METHOD_REFRESH_AUTHORITY = "RefreshAuthority";
    const METHOD_UNVERIFIED_TRANSACTION = "UnverifiedTransactions";
    const SOAP_METHOD_UNVERIFIED_TRANSACTION = "GetUnverifiedTransactions";

    const ERROR_CODES = [
        '-1' =>  'Information submitted is incomplete',
        '-2' =>  'Merchant ID or Acceptor IP is not correct',
        '-3'=>   'Amount should be above 100 Toman',
        '-4'=>   'Approved level of Acceptor is Lower than the silver',
        '-11'=>  'Request Not found',
        '-12'=>  'Request is not editable',
        '-21'=>  'Financial operations for this transaction was not found',
        '-22'=>  'Transaction is unsuccessful',
        '-33'=>  'Transaction amount does not match the paid amount',
        '-34'=>  'Limit the number of transactions or number has crossed the divide',
        '-40'=>  'There is no access to the method',
        '-41'=>  'Additional Data related to information submitted is invalid',
        '-42'=>  'The life span length of the payment ID must be between 30 minutes and 45 days',
        '-54'=>  'Request archived',
        '-998'=> "Connection Error: Can't connect to API (WebService return null)",
        '-999'=> 'Local Library Error',
        '100'=>  'Operation was successful',
        '101'=>  'Operation was successful but PaymentVerification operation on this transaction have already been done.',
    ];
}
