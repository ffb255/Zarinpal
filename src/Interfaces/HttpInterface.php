<?php
namespace ffb128\Zarinpal\Interfaces;

interface HttpInterface
{
    /**
     * Send a post request to a URL.
     *
     * @param  string $method
     * @param  array $postParameters
     * @return Response $body
     */
    public function post(
        $method,
        array $postParameters = [],
        $isSandBox = false
    );
}
