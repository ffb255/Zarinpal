<?php
namespace ffb128\Zarinpal\Http;

use ffb128\Zarinpal\Constants\WebServiceConstants;

class Response
{
    public $ok;
    public $message;
    public $status;
    public $body;

    public function __construct($body) {
        if($body == null){
            $this->ok = false;
            $this->status = "-998";
            $this->message = $this->getMessage($this->status);
            return null;
        }
        $this->body = $body;
        $this->ok = ($this->hasError() ? false : true);
        $this->message = $this->getMessage();
        $this->status = $this->getStatus();
    }

    /**
     * Check is there any error in returned result
     *
     * @return boolean
     */
    private function hasError()
    {
        return ((isset($this->body->Status) && $this->body->Status == 100) ? false : true);
    }

    /**
     * Get returned status (API Result Status Code)
     *
     * @return int
     */
    private function getStatus(){
        return $this->body->Status;
    }

    /**
     * Get and Translate API Message
     *
     * @param string $status optional status code for local errors
     * 
     * @return string
     */
    private function getMessage($status = null){
        if($status != null){
            return WebServiceConstants::ERROR_CODES[$status];
        }
        // Return error message that sent from zarinpal api as $message
        if(isset($this->body->errors)){
            $error = (array) $this->body->errors;
            return reset($error)[0];
        }
        // If there was't any message from zarinpal, use local status translator
        return WebServiceConstants::ERROR_CODES[$this->body->Status];
    }
}
