<?php
namespace ffb128\Zarinpal\Http\Drivers;

use ffb128\Zarinpal\Interfaces\HttpInterface;
use ffb128\Zarinpal\Http\Response;
use ffb128\Zarinpal\Constants\WebServiceConstants;

class Curl implements HttpInterface
{
    /**
     * {@inheritdoc}
     */
    public function post(
        $method,
        array $postParameters = [],
        $isSandbox = false
    ) {
        $url = ($isSandbox ? sprintf(WebServiceConstants::SANDBOX_REST_API_URL, $method) : sprintf(WebServiceConstants::REST_API_URL, $method));
        $request = $this->prepareRequest($url, []);
        $postParametersFiled = json_encode($postParameters);

        curl_setopt($request, CURLOPT_POST, count($postParameters));
        curl_setopt($request, CURLOPT_POSTFIELDS, $postParametersFiled);
        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postParametersFiled)
        ));

        return $this->executeRequest($request);
    }

    /**
     * Prepares a request using curl.
     *
     * @param  string $url [description]
     * @param  array $headers [description]
     * @return resource
     */
    protected static function prepareRequest($url, $headers = [])
    {
        $request = curl_init();

        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLINFO_HEADER_OUT, true);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);

        return $request;
    }

    /**
     * Executes a curl request.
     *
     * @param  resource $request
     * @return Response
     */
    public function executeRequest($request)
    {
        $body = curl_exec($request);

        curl_close($request);
        $body = json_decode($body);
        return new Response($body);
    }
}
