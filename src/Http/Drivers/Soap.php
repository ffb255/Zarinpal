<?php
namespace ffb128\Zarinpal\Http\Drivers;

use ffb128\Zarinpal\Interfaces\HttpInterface;
use ffb128\Zarinpal\Http\Response;
use ffb128\Zarinpal\Constants\WebServiceConstants;

class Soap implements HttpInterface
{
    /**
     * {@inheritdoc}
     */
    public function post(
        $method,
        array $postParameters = [],
        $isSandbox = false
    ) {
        $url = ($isSandbox ? WebServiceConstants::SANDBOX_SOAP_API_URL : WebServiceConstants::SOAP_API_URL);

        // Sorry! I have to do this because of the difference between zarinpal REST and Soap API endpoint!
        if($method == WebServiceConstants::METHOD_UNVERIFIED_TRANSACTION){
            $method = WebServiceConstants::SOAP_METHOD_UNVERIFIED_TRANSACTION;
        }
        
        $client = new \SoapClient($url, ['encoding' => 'UTF-8']);
        $result = $client->$method($postParameters);
        return new Response($result);
    }
}
